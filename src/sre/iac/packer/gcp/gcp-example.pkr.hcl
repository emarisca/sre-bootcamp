variable "machine_type" {
  type    = string
  default = "n1-standard-1"
}

variable "project_id" {
  type    = string
  default = ""
}

variable "region" {
  type    = string
  default = "us-central1"
}

variable "source_image" {
  type    = string
  default = "debian-10-buster-v20210817"
}

variable "zone" {
  type    = string
  default = "us-central1-a"
}

variable "subnetwork" {
  default = "default"
}


locals {
  instance_name = "instance-${formatdate("MM-DD-YYYY", timestamp())}"
}

packer {
  required_plugins {
    googlecompute = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/googlecompute"
    }
  }
}

source "googlecompute" "gcp-ubuntu" {
  disk_size         = 15
  disk_type         = "pd-ssd"
  image_description = "GCP example"
  image_name        = "packer-example-${formatdate("MM-DD-YYYY", timestamp())}"
  machine_type      = var.machine_type
  project_id        = var.project_id
  region            = var.region
  source_image      = var.source_image
  ssh_username      = "packer"
  zone              = var.zone
  network           = "default"
  subnetwork        = var.subnetwork

}


build {
  sources = ["source.googlecompute.gcp-ubuntu"]

  provisioner "shell" {
    script = "scripts/install.sh"
    environment_vars = [ "FOO=msg from packer build" ]
  }

  provisioner "shell" {
    environment_vars = [
      "FOO=hello world",
    ]
    inline = [
      "echo Adding file to Docker Container",
      "echo \"FOO is $FOO\" > example.txt",
    ]
  }

  post-processor "manifest" {
    output     = "manifest.json"
    strip_path = true
    custom_data = {
      my_custom_data = "example"
    }
  }

}
