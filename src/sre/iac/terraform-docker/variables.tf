variable "image_name" {
  description = "input image name to use"
  default     = "nginx:1.18"
}
