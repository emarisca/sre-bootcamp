resource "docker_image" "nginx" {
  name         = var.image_name
  keep_locally = false
}

resource "docker_container" "nginx" {
  count = 3
  image = docker_image.nginx.name
  name  = "terraform-tutorial-${count.index}"
  # ports {
  #   internal = 80
  #   external = 8080
  # }
  labels {
    label = "environment"
    value = "development"
  }

  labels {
    label = "image-txt"
    value = "${docker_image.nginx.name}"
  }
}
